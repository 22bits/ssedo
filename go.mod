module gitlab.com/22bits/ssedo

go 1.17

require (
	github.com/go-resty/resty/v2 v2.6.0
	github.com/rs/zerolog v1.25.0
	github.com/tmaxmax/go-sse v0.4.2
)

require (
	github.com/cenkalti/backoff/v4 v4.1.1 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
)
