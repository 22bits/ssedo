package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tmaxmax/go-sse"
)

var Version string

func main() {
	var outputDir = flag.String("output", "/tmp", "directory in which to write command output files")
	var preserve = flag.Bool("preserve", false, "keep processed entries and command outputs")

	flag.Usage = func() {
		_, _ = fmt.Fprintf(os.Stderr, "Usage: %s [options] <url> <command> [command args]\n\n", filepath.Base(os.Args[0]))
		_, _ = fmt.Fprintf(os.Stderr, "Event data are supplied in the environment to the command.\n\n")
		_, _ = fmt.Fprintf(os.Stderr, "Options:\n\n")
		flag.PrintDefaults()
		_, _ = fmt.Fprintf(os.Stderr, "\n")
	}

	flag.Parse()
	if len(flag.Args()) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	banner := fmt.Sprintf("SSEdo %s", Version)

	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	})

	log.Info().Bool("preserve", *preserve).Msg("keep events and output files")

	sseUrl, err := url.Parse(flag.Args()[0])
	if err != nil {
		log.Fatal().Err(err).Msg("url.Parse")
	}
	command := flag.Args()[1:]

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, sseUrl.String(), nil)
	if err != nil {
		log.Fatal().Err(err).Msg("http.NewRequest")
	}

	req.Header.Set("User-Agent", banner)

	conn := sse.NewConnection(req)
	conn.SubscribeToAll(func(evt sse.Event) {
		// Ignore keep-alive events
		if len(evt.Data) == 0 {
			return
		}

		outputFilename := fmt.Sprintf("ssedo-%s-%s.out", evt.Name, evt.LastEventID)
		outputFilename = filepath.Join(*outputDir, outputFilename)
		outFile, err := os.Create(outputFilename)
		if err != nil {
			log.Fatal().Err(err).Msg("os.Create")
		}

		cmd := exec.Command(command[0], command[1:]...)
		cmd.Env = os.Environ()

		cmd.Stdout = outFile
		cmd.Stderr = outFile

		cmd.Env = append(cmd.Env, fmt.Sprintf("STREAM_NAME=%s", evt.Name))
		cmd.Env = append(cmd.Env, fmt.Sprintf("STREAM_EVENT_ID=%s", evt.LastEventID))

		// sseredis separates key/value pairs with newlines and keys from values with an equal
		lines := strings.Split(string(evt.Data), "\n")
		for _, line := range lines {
			chunks := strings.SplitN(line, "=", 2)
			key := strings.ToUpper(chunks[0])
			val := chunks[1]

			cmd.Env = append(cmd.Env, fmt.Sprintf("EVENT_%s=%s", key, val))
		}

		log.Info().Str("stream", evt.Name).Str("event id", evt.LastEventID).Msg("executing command")

		// Run command
		err = cmd.Run()
		if err != nil {
			log.Fatal().Err(err).Msg("cmd.Run")
		}

		log.Info().Str("stream", evt.Name).Str("event id", evt.LastEventID).Msg("command succeeded")

		err = outFile.Close()
		if err != nil {
			log.Fatal().Err(err).Msg("outFile.Close")
		}

		if *preserve {
			return
		}

		// Clean up output file
		err = os.Remove(outputFilename)
		if err != nil {
			log.Fatal().Err(err).Msg("os.Remove")
		}

		// Delete event from stream
		client := resty.New()

		deleteUrl, err := sseUrl.Parse(fmt.Sprintf("%s/%s", evt.Name, evt.LastEventID))
		if err != nil {
			log.Fatal().Err(err).Msg("sseUrl.Parse")
		}
		res, err := client.R().SetContext(ctx).SetHeader("User-Agent", banner).Delete(deleteUrl.String())
		if err != nil {
			log.Fatal().Err(err).Msg("resty.Delete")
		}

		if res.StatusCode() != 200 {
			log.Fatal().Str("res.Status", res.Status()).Msg("delete status")
		}
	})

	log.Info().Str("version", Version).Msg("starting")

	err = conn.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("conn.Connect")
	}

	select {
	case <-ctx.Done():
		log.Warn().Msg("shutting down")
	}
}
