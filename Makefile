GIT_VERSION := $(shell git describe --abbrev=10 --dirty --always --tags)

.PHONY: build clean

build:
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -X main.Version=${GIT_VERSION}'

clean:
	go clean
